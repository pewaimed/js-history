var History = function(settings){
	
	var _currentStateName = false;
	
	var _settings = settings ? settings:{};
	
	// set fallback modifier
	var _modifier = _settings.modifier ? "#" + _settings.modifier: "#!";
		
	// check if history is supported
	var _supportsPushState = (!('pushState' in window.history) || _settings.html5==false) ? false:true;
				
	// if pushstate is not supported, check if location has hash, if not, add it
	if(!_supportsPushState && !window.location.hash)
	
		window.location.replace("/"+_modifier+window.location.pathname);
			
	else if(window.location.hash && _supportsPushState)
	 
		window.history.replaceState({},"",window.location.hash.replace(_modifier,"").replace("#",""));
	
	// statename function
	var stateName = function(){
		
		return (_supportsPushState) ? window.location.pathname:window.location.hash.substr(_modifier.length);
		
	} 
	
	// initialize current state
	_currentStateName = stateName();
	
	// trigger function
	var trigger = function(){
		
		if(typeof(window.statechange) == 'function') window.statechange();
					
	} 


	// checkstate function
	var checkState = function(_pushed){
				
		// check if state has changed
		if(_currentStateName != stateName()){

			//trigger the global statechange event
			trigger();
			
			_currentStateName = stateName();
			
		}

	}
	
	// check which event is avaliable, else use intervall fallback
	if('onpopstate' in window){
		
		// bind popstate event
		window.onpopstate = function(){
			checkState();
		}
		
	} else if('onhashchange' in window){
		
		// bind hashchangeevent
		window.onhashchange = function(){
			checkState();
		}
				
	} else {
		
		// set interval
		setInterval(function(){
			checkState();
		},
		250);
		
	}
	
	// pushstate function	
	this.pushState = function (title,url){
		
		// check if title was passed
		if(!url){
			url = title;
			title = false;
		}
		
		// make sure url begins with slash
		url = (url[0]=="/") ? url:"/"+url;
		
		// trivial check
		if(stateName() != url){
			
			// this was pushed
			_pushed = true;
			
			// actually push state
			if(_supportsPushState){
				
				window.history.pushState(
					{},
					"",
					url ? url:""
				);
				
			} else {
				
				window.location=_modifier+url;
				
				// if title was passed, change it manually
				if(title)
				
					document.getElementsByTagName("title").item(0).innerHTML = title;
				
				
			}
			
			// manually trigger checkstate
			checkState(true);
			
		}
		
	}
	
	
	// get _currentState
	this.getState = function (){
		
		return {object:{},title:document.getElementsByTagName("title").item(0).innerHTML,url:stateName()};
		
	}
	
	trigger();
	
}